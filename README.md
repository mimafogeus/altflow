[![Build status](https://ci.appveyor.com/api/projects/status/d3s5vedr6dwuc7yh/branch/master?svg=true)](https://ci.appveyor.com/project/wildeyes/altflow/branch/master)

# AltFlow

An outlining app that aligns with how you think.

_[Latest working version here](altflow.hackd.now.sh). WIP_

## Developing Locally / Contributing

See CONTRIBUTING.md.

## Bugs / Help

Use the issue tracker to ask questions about the code, report bugs and get help.

## Contributing

see CONTRIBUTING.md

## Alternatives

- [Workflowy](https://workflowy.com/online-notepad/)
- [Dynalist](https://dynalist.io/)

## Contributers

- Me
- You maybe? :)
