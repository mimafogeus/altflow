# Helping out

Thank you for considering to help out with Altflow!

Any help with design (logo, app itself) would be appreciated. For coding help;

## Development Instructions

To install and develop the app locally;

- `git clone https://github.com/wildeyes/AltFlow`
- `cd altflow`
- `yarn` or `npm install`
- `yarn start` or `npm start`

## What to work on.

Here's a roadmap in README.md. If you'd like to implement something on your own go ahead and do so but preferably talk with me before.

## Next Feature Roadmap

- add support for expressions in notes.
- add support for "last, ago" expressions.
- add support for highlighting parts (expressions, tags, at signs).
- improve expression tab design and add animation.
- Search.
- Hashtags.
- ctrl-z.
- daily summary email.
- markdown formatting.
- "Share" bullets via email.
- Basic design.
- Basic animations.
- Themes / Custom CSS.
- Keyboard shortcuts pop out panel for reference.
- Starring documents.

#### More Ideas

- Flutter Apps
  - Integrate with Siri
  - Integrate with Google Assistant (Help required)
- Expose outward API to allow plugins via a userscript like interface.
- Plugin Store, easy way to download, remove and preview plugins.
- iCal integration.
