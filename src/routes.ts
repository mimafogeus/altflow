import { createBrowserHistory } from 'history';
import { Node } from './lib/models/Node/Node';

export const history = createBrowserHistory();

// this is needed for now because of the annoying isHome differentiation
export function pushBlock(node?: Node) {
  if (!node || node.isHome) history.push('/home')
  else history.push('/app/node/' + node.id)
}
