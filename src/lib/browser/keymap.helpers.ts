import React from 'react'
import { Selection } from '../types'

export function extractSelection(
	event: React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
) {
	const { target } = event as any
	const {
		selectionStart: _start,
		selectionEnd: _end,
		selectionDirection: _direction,
	} = target
	return [_start || 0, _end || 0, _direction || 'none'] as Selection
}
