/** whether we're in the input or textarea */
export enum BulletFocus {
	Title = 'title',
	Notes = 'notes',
}
/** Type to allow persisting complex selection instead of just focus() */
export type Selection = [number, number, 'forward' | 'backward' | 'none']
/** has boolean to allow unfocused (false) or simply .focus() (true) */
export type ShouldFocus = null | { type: BulletFocus; selection?: Selection }
export type NodeJSONType = {
	title: string
	notes: string | null
	created: string
	modified: string
	starred: boolean
	completed: boolean
	children: NodeJSONType[]
}
export type Mouse = { x: number; y: number }
export type DROP_POS = 'TOP' | 'BOTTOM' | 'CHILDREN'
