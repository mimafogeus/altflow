import { Appwrite } from 'appwrite'

export const appwrite = new Appwrite()

appwrite
	.setEndpoint(process.env.REACT_APP_API_URL)
	.setProject(process.env.REACT_APP_PROJECT)

const collectionIds = {
	nodes: process.env.REACT_APP_NODES_COLLECTION_ID,
}

let account = null

export const loadAccount = async () => {
	return (account = await appwrite.account.get())
}

export const getData = async () => {
	const data = await appwrite.database.getDocument<{
		email: string
		nodes: string
	}>(collectionIds.nodes, account.prefs.nodes)

	return JSON.parse(data.nodes)
}

export const writeNodes = async nodesStr => {
	await appwrite.database.updateDocument(
		collectionIds.nodes,
		account.prefs.nodes,
		{
			userId: account.email,
			nodes: nodesStr,
		}
	)
}
if (process.env.NODE_ENV === 'development') {
	window['appwrite'] = appwrite
	window['collectionIds'] = collectionIds
}

export const signup = async (name: string, email: string, password: string) => {
	await appwrite.account.create(email, password, name)
	await appwrite.account.createSession(email, password)
	// this section should reside in a reaction-function in appwrite but it doesnt work,
	// so it's here for awhile
	const { $id } = await appwrite.database.createDocument(collectionIds.nodes, {
		userId: email,
		nodes: JSON.stringify([]),
		// TODO add permissions
	})

	await appwrite.account.updatePrefs({ nodes: $id })
	await loadAccount()
}
