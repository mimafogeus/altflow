import React from 'react'
import { observer } from 'mobx-react'
import { useInject } from '../../stores/root.provider'
import { Route, Redirect } from 'react-router-dom'

export const PrivateRoute = observer(({ children, ...rest }) => {
	const auth = useInject(rootStore => rootStore.authManager)

	if (auth.isLoading) return null

	return (
		<Route
			{...rest}
			render={({ location }) =>
				auth.user ? (
					children
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: location },
						}}
					/>
				)
			}
		/>
	)
})
