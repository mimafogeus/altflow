/**
 * only supports he-il ATM, feel free to add
 */
export function detectRtl(str: string) {
	const he = str.match(/[\u05d0-\u05ea]/)
	const en = str.match(/[a-zA-Z]/)
	return he !== null && en !== null
		? he.index < en.index
		: en === null && he !== null
}
