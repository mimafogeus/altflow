import React, { useState, useEffect } from 'react'
import classnames from 'classnames'

import AutosuggestHighlightMatch from 'autosuggest-highlight/umd/match'
import AutosuggestHighlightParse from 'autosuggest-highlight/umd/parse'

import './CtrlK.scss'
import Autosuggest from 'react-autosuggest'
import rootStore from '../../stores/root'
import { Node } from '../../models/Node/Node'
import { detectRtl } from '../../helpers/rtl.helpers'

interface CtrlKProps {}

export const CtrlK = ({}: CtrlKProps) => {
	const [value, setValue] = useState('')
	const [suggestions, setSuggestions] = useState([])

	useEffect(() => {
		document.querySelector<HTMLDivElement>('.CtrlK__input').focus()
	})

	function onChange(event, { newValue = '', method }) {
		setValue(newValue)
	}

	function onSuggestionsFetchRequested({ value }) {
		setSuggestions(getSuggestions(value))
	}

	const inputProps = {
		placeholder: 'Jump to...',
		value,
		onChange: onChange,
	}

	return (
		<div className={classnames('CtrlK')}>
			<div
				className="CtrlK__overlay"
				onClick={e => {
					rootStore.uiManager.isCtrlKOpen = false
				}}
			/>
			<Autosuggest
				alwaysRenderSuggestions={true}
				highlightFirstSuggestion={true}
				theme={{
					container: 'CtrlK__container CtrlK__container--open',
					containerOpen: 'CtrlK__container--open',
					input: classnames('CtrlK__input', {
						'--rtl-semi': detectRtl(value),
					}),
					inputOpen: 'CtrlK__input--open',
					inputFocused: 'CtrlK__input--focused',
					suggestionsContainer: 'CtrlK__suggestions-container',
					suggestionsContainerOpen: 'CtrlK__suggestions-container--open',
					suggestionsList: 'CtrlK__suggestions-list',
					suggestion: 'CtrlK__suggestion',
					suggestionFirst: 'CtrlK__suggestion--first',
					suggestionHighlighted: 'CtrlK__suggestion--highlighted',
					sectionContainer: 'CtrlK__section-container',
					sectionContainerFirst: 'CtrlK__section-container--first',
					sectionTitle: 'CtrlK__section-title',
				}}
				suggestions={suggestions}
				onSuggestionsFetchRequested={onSuggestionsFetchRequested}
				onSuggestionsClearRequested={() => {}}
				getSuggestionValue={() => value}
				renderSuggestion={renderSuggestion}
				onSuggestionSelected={(formEvent, autosuggestEvent) => {
					const { suggestion: node } = autosuggestEvent

					rootStore.uiManager.setDoc(node)
					rootStore.uiManager.isCtrlKOpen = false
				}}
				inputProps={inputProps}
			/>
		</div>
	)
}

function getSuggestions(value: string) {
	if (!value || !value.length) return []

	function recurse(node: Node) {
		const result = filter(node)

		const children = node.children.flatMap(recurse)

		return result ? [node, ...children] : children
	}

	function filter(node: Node) {
		return node.title.indexOf(value) !== -1
	}

	const nodes = rootStore.dataStore.home.children.flatMap(recurse)

	return nodes
}

function renderSuggestion(node: Node, { query }) {
	const matches = AutosuggestHighlightMatch(node.title, query)
	const parts = AutosuggestHighlightParse(node.title, matches) || []

	return (
		<>
			<div className="CtrlK__parent-content">{node.parent?.title}</div>
			<div
				className={classnames('CtrlK__suggestion-content', {
					'CtrlK__suggestion-content--rtl-semi': detectRtl(parts[0]?.text),
				})}
			>
				{parts.map((part, index) => {
					return (
						<div
							className={classnames('CtrlK__part', {
								'CtrlK__part--rtl': detectRtl(part.text),
								'CtrlK__part--match': part.highlight,
							})}
							key={index}
						>
							{part.text}
						</div>
					)
				})}
			</div>
		</>
	)
}
