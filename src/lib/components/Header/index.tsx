import React from 'react'
import classnames from 'classnames'
import { Breadcrumbs } from './Breadcrumbs'
import { observer } from 'mobx-react'
import { useInject } from '../../stores/root.provider'

import './Header.scss'

export const Header = observer(() => {
	const auth = useInject(rootStore => rootStore.authManager)
	const uiManager = useInject(rootStore => rootStore.uiManager)

	return (
		<div className={classnames('Header')}>
			{/* <button className="starred">starred</button> */}
			<Breadcrumbs />
			<button className="showCompleted">Show completed</button>
			<button
				className="darkmode"
				onClick={() => (uiManager.darkmode = !uiManager.darkmode)}
			>
				Dark Mode
			</button>
			<button className="rtl" onClick={() => (uiManager.rtl = !uiManager.rtl)}>
				RTL
			</button>

			<button onClick={() => auth.logout()}>Logout</button>
			<div
				className="g-signin2"
				data-onsuccess="onSignIn"
				data-theme="dark"
			></div>

			{/* <button className="search">search</button>
				<button className="settings">settings</button> */}
		</div>
	)
})
