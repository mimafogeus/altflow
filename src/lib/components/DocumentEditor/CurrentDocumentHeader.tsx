import classnames from 'classnames'
import { observer } from 'mobx-react'
import React, { useRef, useState } from 'react'
import { Textarea } from '../../common'
import { useInject } from '../../stores/root.provider'
import { useShouldFocus } from '../Bullet/hooks'
import { detectRtl } from '../../helpers/rtl.helpers'
import { format } from 'date-fns'

export const CurrentDocumentHeader = observer(() => {
	const uiManager = useInject(rootStore => rootStore.uiManager)
	const node = uiManager.doc

	const titleRef = useRef<HTMLInputElement>(null)
	const notesRef = useRef<HTMLTextAreaElement>(null)

	const expression = useState<string>(null)

	useShouldFocus(node, titleRef, notesRef)
	return (
		<section className="doc">
			<input
				className={classnames('doc__title title-input', {
					'--rtl-semi': detectRtl(node.title),
				})}
				onChange={({
					currentTarget: { value, selectionStart: cursorIndex },
				}: React.FormEvent<HTMLInputElement>) => {
					console.log(value.length, cursorIndex)
					return (node.title = value || '')
				}}
				// onBlur={() => closeExpressionTab()}
				value={node.title}
				placeholder="Untitled"
				ref={titleRef}
			/>
			{node.shouldDisplayNotes && (
				<Textarea
					className={classnames('doc__notes notes-textarea', {
						'--rtl-semi': detectRtl(node.title),
					})}
					onKeyDown={(e: React.FormEvent) => {}}
					value={node.notes!}
					inputRef={notesRef}
				/>
			)}
		</section>
	)
})
