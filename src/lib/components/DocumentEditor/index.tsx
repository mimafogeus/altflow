import { observer } from 'mobx-react'
import React from 'react'
import { FLAG_HOME, focusTitle } from '../../models/Node/Node'
import { useInject } from '../../stores/root.provider'
import { AddChildBtn } from '../AddChildBtn/AddChildBtn'
import { Bullet } from '../Bullet/Bullet'
import { CurrentDocumentHeader } from './CurrentDocumentHeader'
import './DocumentEditor.scss'

export const DocumentEditor = observer(() => {
	const uiManager = useInject(rootStore => rootStore.uiManager)

	return (
		<section
			className="DocumentEditor"
			style={{ direction: uiManager.rtl ? 'rtl' : 'ltr' }}
		>
			{uiManager.doc && !uiManager.doc.isHome && <CurrentDocumentHeader />}
			{uiManager.doc.children.map((b, i) => (
				<Bullet rtl={uiManager.rtl} node={b} key={i} />
			))}
			<AddChildBtn
				onClick={() => uiManager.doc.createChild({ shouldFocus: focusTitle })}
			/>
		</section>
	)
})
