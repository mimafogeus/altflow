import 'animate.css'
import classnames from 'classnames'
import { observer } from 'mobx-react-lite'
import React, { useEffect, useRef, useState } from 'react'
import { animateRtl, Textarea } from '../../common'
import { detectRtl } from '../../helpers/rtl.helpers'
import { Node } from '../../models/Node/Node'
import { pushBlock } from '../../../routes'
import { useInject } from '../../stores/root.provider'
import { Mouse, BulletFocus } from '../../types'
import { AddChildBtn } from '../AddChildBtn/AddChildBtn'
import { useShouldFocus } from './hooks'
import { Chevron } from './Chevron'

import './Bullet.scss'
import './expression-tab.scss'
import { useExpressionTab } from '../../managers/expressiontab.manager'

export const Bullet = observer(
	({ node, rtl }: { rtl: boolean; node: Node }) => {
		const uiManager = useInject(rootStore => rootStore.uiManager)
		const titleRef = useRef<HTMLInputElement>(null)
		const notesRef = useRef<HTMLTextAreaElement>(null)
		const [overline, setOverline] = useState(false)
		// grabbing and dnd
		const [mousePos, setMousePos] = useState<Mouse | null>(null)
		const [dropMaybeMe, dropPos] = uiManager.droppingStatus || [null, 'TOP']
		const [hasFocus, setFocus] = useState<boolean>()

		const addedToSelection = uiManager.isMultipleSelected(node)
		const {
			expressionValue,
			dummyInputRef,
			expressionTabRef,
			placeExpression,
		} = useExpressionTab(titleRef)

		useShouldFocus(node, titleRef, notesRef)

		return (
			<div
				className={classnames('bullet', {
					completed: node.completed,
					starred: node.starred,
					overline: overline && !uiManager.isDnd,
					addedToSelection,
					open: node.open,
					children: node.children.length,
				})}
			>
				<div
					className={classnames('bullet__content', {
						'bullet__dnd-top': dropMaybeMe === node && dropPos === 'TOP',
						'bullet__dnd-bottom': dropMaybeMe === node && dropPos === 'BOTTOM',
					})}
				>
					<div
						className="bullet__chevron"
						onClick={() => (node.open = !node.open)}
					>
						<Chevron />
					</div>
					<div
						className={classnames('bullet__bullet', {
							grabbing: Boolean(mousePos),
							overline: overline && !uiManager.isDnd,
						})}
						onMouseUp={() => !mousePos && pushBlock(node)}
						onMouseEnter={() => setOverline(true)}
						onMouseLeave={() => setOverline(false)}
						onMouseDown={e => {
							uiManager.startDnd(node, setMousePos)
							e.preventDefault()
						}}
						style={
							mousePos
								? {
										position: 'fixed',
										top: mousePos.y,
										left: mousePos.x,
										transform: 'translate(-50%, -50%)',
								  }
								: undefined
						}
					/>
					<input
						className={classnames('bullet__title title-input', {
							[`animated ${animateRtl('fadeOut', rtl)}`]: node.completed,
							'--rtl-semi': detectRtl(node.title),
							'bullet__dnd-children':
								dropMaybeMe === node && dropPos === 'CHILDREN',
						})}
						data-id={node.id}
						value={node.title}
						onChange={({
							currentTarget: { value },
							target: { selectionStart: cursorIndex },
						}) => {
							placeExpression(value, cursorIndex)
							return (node.title = value)
						}}
						ref={titleRef}
						onBlur={() => {
							setFocus(false)
							uiManager.unsetCurrent()
						}}
						onFocus={event => {
							const {
								currentTarget: { value },
								target: { selectionStart: cursorIndex },
							} = event
							uiManager.setCurrent(node)
							node.focusedOn = BulletFocus.Title
							placeExpression(value, cursorIndex)
							setFocus(true)
						}}
						onMouseDown={() => {
							uiManager.clearMultipleSelect()
							uiManager.startMultipleSelect(node)
						}}
					/>

					<input
						ref={dummyInputRef}
						style={{
							visibility: 'hidden',
							position: 'absolute',
						}}
						className={'bullet__title start-second-row'}
					/>
					<div
						ref={expressionTabRef}
						className="expression-tab bullet__title"
						style={{
							position: 'absolute',
							display: hasFocus && expressionValue ? 'inherit' : 'none',
						}}
					>
						<div className="expression-value">{expressionValue}</div>
						<div className="expression-instructions">
							hit <span className="expression-tab-tab">tab</span> to set date
						</div>
					</div>
					{node.shouldDisplayNotes && (
						<Textarea
							className={classnames('bullet__notes notes-textarea', {
								'--rtl-semi': detectRtl(node.title),
							})}
							onChange={({ currentTarget: { value } }) => (node.notes = value)}
							value={node.notes!}
							inputRef={notesRef as any}
							onBlur={() => uiManager.unsetCurrent()}
							onFocus={() => {
								uiManager.setCurrent(node)
								node.focusedOn = BulletFocus.Notes
							}}
						/>
					)}
				</div>

				{Boolean(node.children.length) && (
					<div className="bullet__children">
						{node.children.map((b, i) => (
							<Bullet rtl={rtl} node={b} key={i} />
						))}
						<AddChildBtn
							onClick={() => node.createChild({}, true)}
							className="bullet__addChildBtn"
						/>
					</div>
				)}
			</div>
		)
	}
)
