import { observer } from 'mobx-react'
import React from 'react'
import { Link, Redirect, useHistory, useLocation } from 'react-router-dom'
import { useInject } from '../../stores/root.provider'
import { UserAuth } from './UserAuth'
import { ErrorMessage, Field, Form, Formik, useFormik } from 'formik'
import LaddaButton, { EXPAND_RIGHT } from 'react-ladda-button'

export const RegisterPage = observer(() => {
	const auth = useInject(rootStore => rootStore.authManager)
	const history = useHistory()
	const location = useLocation()
	const { from } = (location as any).state || { from: { pathname: '/app' } }

	const onSendForm = async (name: string, email: string, password: string) => {
		await auth.register(name, email, password)
		auth.signin(email, password)
	}
	const onSuccess = () => {
		history.replace(from)
	}

	if (auth.user) {
		return (
			<Redirect
				to={{
					pathname: '/app',
				}}
			/>
		)
	}

	return (
		<div>
			<h1>AltFlow</h1>
			<Link to="/login">Login instead?</Link>
			<h2>Register Form</h2>
			<Formik
				initialValues={{ name: '', email: '', password1: '', password2: '' }}
				validate={values => {
					const errors: any = {}
					if (!values.email) {
						errors.email = 'Required'
					} else if (
						!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
					) {
						errors.email = 'Invalid email address'
					}
					if (values.password1 !== values.password2) {
						errors.password2 = 'Passwords not the same'
					}
					return errors
				}}
				onSubmit={async (
					{ name, email, password1, password2 },
					{ setErrors, setSubmitting }
				) => {
					try {
						await onSendForm(name, email, password1)

						setSubmitting(false)
						onSuccess()
					} catch (err) {
						if (err.message?.includes('password'))
							setErrors({ password1: err.message })
						else {
							setErrors({ password2: err.message })
							console.warn(err)
						}
						setSubmitting(false)
					}
				}}
			>
				{({ isSubmitting }) => (
					<Form translate={{}}>
						<Field type="name" name="name" placeholder="name" />
						<ErrorMessage name="name" component="div" />
						<Field type="email" name="email" placeholder="email" />
						<ErrorMessage name="email" component="div" />
						<Field type="password" name="password1" placeholder="password" />
						<ErrorMessage name="password1" component="div" />
						<Field
							type="password"
							name="password2"
							placeholder="Same password"
						/>
						<ErrorMessage name="password2" component="div" />
						<ErrorMessage name="general" component="div" />
						<LaddaButton loading={isSubmitting} data-style={EXPAND_RIGHT}>
							Submit
						</LaddaButton>
					</Form>
				)}
			</Formik>
		</div>
	)
})
