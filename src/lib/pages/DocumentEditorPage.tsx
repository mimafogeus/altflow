import classnames from 'classnames'
import { observer } from 'mobx-react'
import React, { useEffect, useState } from 'react'
import { DocumentEditor } from '../components/DocumentEditor'
import { Header } from '../components/Header'
import { match, useRouteMatch } from 'react-router-dom'
import { useInject } from '../stores/root.provider'
import { useAutorun } from '../hooks'
import { LoadingSpinner } from '../components/LoadingSpinner'
import { CtrlK } from '../components/CtrlK/CtrlK'

const DocumentEditorPage: React.FC = observer(() => {
	const dataStore = useInject(rootStore => rootStore.dataStore)
	const uiManager = useInject(rootStore => rootStore.uiManager)
	const [loaded, setLoaded] = useState(dataStore.loaded)
	const isCtrlKOpen = uiManager.isCtrlKOpen
	const route = useRouteMatch<{ id: string }>()
	// please fix this horrible hack
	useEffect(() => {
		setLoaded(dataStore.loaded)
	})
	// also this
	useEffect(() => {
		function handleCurrentDocumentRespond({
			path,
			params,
		}: match<{ id: string }>) {
			// respond to changes in current doc
			if (path === '/app') {
				uiManager.setDoc()
			} else if (path === '/app/node/:node') {
				const { id } = params
				const node = dataStore.nodes[id]

				uiManager.setDoc(node)
			}
		}
		// run on page load, once
		if (loaded) {
			handleCurrentDocumentRespond(route)
			// TODO respond to path changes?
		}
	}, [loaded])

	if (!dataStore.loaded) return <LoadingSpinner />

	return (
		<div
			className={classnames('app', {
				grabbing: uiManager.isDnd,
				darkmode: uiManager.darkmode,
			})}
			onMouseUp={() => {
				uiManager.endDnd()
				uiManager.stopMultipleSelect()
			}}
			onMouseMove={({ clientX: x, clientY: y }) => {
				if (uiManager.isDnd) {
					uiManager.moveDnd({ x, y }) // report mouse pos to update bullet pos
				}
				if (uiManager.isMultipleSelecting) {
					uiManager.moveMultipleSelect({ x, y })
				}
			}}
		>
			<Header />
			<DocumentEditor />
			{isCtrlKOpen && <CtrlK />}
		</div>
	)
})
export default DocumentEditorPage
