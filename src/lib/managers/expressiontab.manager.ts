import { format, add, sub, } from 'date-fns';
import { action, computed, observable } from 'mobx';
import { useRef } from 'react';
import { Node } from '../models/Node/Node';
import { useInject } from '../stores/root.provider';
import { BulletFocus } from '../types';

export default class ExpressionTabManager {
  cursorIndex: number
  @observable expression: string
  @observable expressionValue: string

  @computed get isBeingSuggested() {
    return !!this.expression
  }

  @action acceptSuggestion(node: Node) {
    const value = node.focusedOn === BulletFocus.Title ? node.title : node.notes
    const newValue = value.slice(0, this.cursorIndex - this.expression.length) + this.expressionValue + value.slice(this.cursorIndex, value.length)

    if (node.focusedOn === BulletFocus.Title)
      node.title = newValue
    else
      node.notes = newValue

    this.expression = null
    this.expressionValue = null
  }

  @action clear() {
    this.expression = null
    this.expressionValue = null
    this.cursorIndex = null
  }
}

const dateFormat = 'eee, MMM yyyy'
function getExpression(value: string, cursorIndex: number) {
  /**
   * Matches things like:
    today at 1pm, monday at 2am
    tomorrow at 3PM, in two weeks at 4AM
    four weeks ago at 12:34
   */
  const fluentDatesRegex = /(?<hasIn>(in) )?(?<last>(last) )?(?:(?<named>today|tomorrow|yesterday)|(?<day_of_week>sunday|monday|tuesday|wednesday|thursday|friday|saturday)|(?<number_quantifier>one|two|three|four|\d\d?\d?) (?<quantifier_modifier>seconds|minutes|hours|weeks|days|months|years))(?<ago> ago)?(?: at (?:(?:(?<hours1>\d\d?):(?<minutes>\d\d))|(?<hours2>\d\d?)) ?(?<ampm>am|pm|AM|PM)?)?/gi;
  const [...matches] = value.slice(0, cursorIndex).matchAll(fluentDatesRegex)
  if (!matches.length) return null
  let closeness = Infinity
  let closestMatch: typeof matches[number];
  matches.forEach((match, index) => {
    if (Math.abs(match.index - cursorIndex) < closeness) {
      closestMatch = match
      closeness = Math.abs(match.index - cursorIndex)
    }
  })
  const { last, hasIn, named, day_of_week, number_quantifier, quantifier_modifier, ago, hours1, hours2, minutes, ampm } = closestMatch.groups

  let date;
  if (hasIn && number_quantifier) {
    const op = hasIn ? add : sub
    const number = isNaN(parseInt(number_quantifier)) ? {
      one: 1,
      two: 2,
      three: 3,
      four: 4
    }[number_quantifier] : number_quantifier
    date = op(new Date, {
      [quantifier_modifier]: number
    })
  }
  // TODO
  /* else if(ago && number_quantifier) {

  } else if(last && day_of_week) {

  }*/ else if (named || day_of_week) {
    if (named === 'today') date = new Date
    if (named === 'tomorrow') date = add(new Date, { days: 1 })
    if (named === 'yesterday') date = sub(new Date, { days: 1 })
  }
  if (date)
    return {
      expression: closestMatch[0],
      expressionValue: format(date, dateFormat),
    }
}

export function useExpressionTab(inputRef) {
  const exprtab = useInject(rootStore => rootStore.uiManager.exprtab)

  const dummyInputRef = useRef<HTMLInputElement>(null)
  const expressionTabRef = useRef<HTMLDivElement>(null)

  return {
    expressionValue: exprtab.expressionValue,
    dummyInputRef,
    expressionTabRef,
    placeExpression: (value, cursorIndex) => {
      exprtab.cursorIndex = cursorIndex
      const result = getExpression(value, cursorIndex)
      if (!result) {
        exprtab.clear()
      } else {
        const { expressionValue, expression } = result
        // now, to place the expressionTab right below the expression typed
        // By calculating the width of the sentence (possibly in the middle of the text),
        // subtracted by the width of the expression divided by two to place it in the middle
        const sentence = value.slice(0, cursorIndex)
        // get sentence width
        dummyInputRef.current.value = sentence
        dummyInputRef.current.style.width = sentence.length + 'ch'
        const sentenceWidth = dummyInputRef.current.getBoundingClientRect()
          .width
        // get expression width
        dummyInputRef.current.value = expression
        dummyInputRef.current.style.width = expression.length + 'ch'
        const {
          width: expressionWidth,
        } = dummyInputRef.current.getBoundingClientRect()
        // generate x coordinates for expressionTab
        const width = sentenceWidth - expressionWidth / 2

        expressionTabRef.current.style.left = width + 'px'
        // use titleRef because dummyInputRef is absolutely positioned
        const { height } = inputRef.current.getBoundingClientRect()

        expressionTabRef.current.style.top = height + 'px'

        exprtab.expression = expression
        exprtab.expressionValue = expressionValue
      }
    },
  }
}