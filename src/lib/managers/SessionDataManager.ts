import { observable, extendObservable, autorun } from 'mobx'

export default class SessionDataManager {
	static Instance: SessionDataManager

	@observable opens = {}

	constructor() {
		let data = {
			opens: {},
		}
		try {
			const ls = localStorage.getItem('sessionDataManager')
			data = JSON.parse(ls)
			extendObservable(this.opens, data.opens)
			// console.log(this.opens)
		} catch (e) {
			console.error(e)
		}
		// console.log('sessionDataManager localStorage data', data.opens, this.opens)
		autorun(() => {
			localStorage.setItem(
				'sessionDataManager',
				JSON.stringify({ opens: this.opens })
			)
		})

		window['SessionDataManager'] = SessionDataManager.Instance = this
	}
}
