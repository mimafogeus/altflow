import DataStore from './data.store'
import UIManager from './ui.manager'
import AuthManager from './auth.manager'
import StorageManager from './storage.manager'
import KbsManager from './kbs/kbs.manager'
import SessionDataManager from '../managers/SessionDataManager'
import { autorun } from 'mobx'

export class RootStore {
	authManager: AuthManager
	storageManager: StorageManager
	dataStore: DataStore
	uiManager: UIManager
	kbsManager: KbsManager
	sessionDataManager: SessionDataManager
	constructor() {
		this.authManager = new AuthManager()

		this.authManager.checkAccount()

		autorun(() => console.log('user', this.authManager.user))
		autorun(() => console.log('isLoading', this.authManager.isLoading))
		autorun(() => console.log('isLoggedIn', this.authManager.isLoggedIn))

		this.sessionDataManager = new SessionDataManager()
		this.storageManager = new StorageManager(this.authManager)
		this.dataStore = new DataStore(this.authManager, this.storageManager)
		this.uiManager = new UIManager(this.dataStore)
		this.kbsManager = new KbsManager(this.uiManager)
	}
}

const rootStore = new RootStore()

if (process.env.NODE_ENV === 'development') {
	; (window as any).rootStore = rootStore
}

export default rootStore
