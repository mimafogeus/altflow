import AuthManager from './auth.manager'
import { observable, autorun } from 'mobx'
import { getData, writeNodes } from '../integrations/appwrite/appwrite'
import { delay } from '../helpers'
import { debounce } from 'lodash'

const remoteFetchTimeout = 10e3

export default class StorageManager {
	@observable isDataAvailable = false
	@observable isLoading = false
	data: any

	constructor(private authManager: AuthManager) {
		autorun(() => {
			if (authManager.isLoggedIn) {
				this.fetch()
			}
		})
	}
	async fetch(): Promise<any> {
		this.isLoading = true
		const text = localStorage.getItem('dataStore')

		if (text) {
			this.data = JSON.parse(text)
			this.isDataAvailable = true
		}

		// TODO consolidate data
		// async fetch
		const fetch = Promise.race([
			getData(),
			delay(remoteFetchTimeout)
		])
			.then(data => {
				if (data) {
					this.isDataAvailable = true
					return this.data = data
				}
				else {
					// TODO timeout
				}
			})

		fetch.then(() => {
			this.isLoading = false
		})

		return fetch
	}
	put = debounce(async (key: string, nodes: any) => {
		const data = JSON.stringify(nodes)
		// save locally
		localStorage.setItem(key, data)
		// save remotely
		writeNodes(data)
	}, 3e3)
}
