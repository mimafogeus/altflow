import hotkeys from 'hotkeys-js'
import { observable } from 'mobx'
import bulletActions from './bulletActions'
import UIManager from '../ui.manager'
import defaultKeymap from './keymap'
import globalActions from './globalActions'

export default class KbsManager {
	@observable keymap: { [action: string]: string } = {}
	splitKey = '-'

	constructor(private uiManager: UIManager) {
		this.keymap = defaultKeymap
		this.load()
	}
	load() {
		hotkeys.filter = () => true // enable hotkeys on input and textarea

		for (const [scope, actionNameToActionFn] of Object.entries(whenToAction)) {
			// add globally available keys to every scope
			for (const [actionName, actionFn] of Object.entries(globalActions)) {
				const keyCombination = this.keymap[actionName]
				// console.log(scope, keyCombination, actionName, actionFn)
				hotkeys(
					keyCombination,
					{ splitKey: this.splitKey, scope },
					(event, handler) => {
						actionFn(this.uiManager.current)
					}
				)
			}

			for (const [actionName, actionFn] of Object.entries(
				actionNameToActionFn
			)) {
				const keyCombination = this.keymap[actionName]
				hotkeys(
					keyCombination,
					{ splitKey: this.splitKey, scope },
					(event, handler) => {
						actionFn(this.uiManager.current, event as any, this.uiManager)
					}
				)
			}
		}
		setScope('global')
	}
}

export function setScope(scope: When) {
	// console.log('scope', scope)
	hotkeys.setScope(scope)
}

type Action = keyof typeof bulletActions

const whenToAction = {
	'editing-bullet': bulletActions,
	global: {}, // no need to add globals here since they get added on every scope
}

type When = keyof typeof whenToAction
