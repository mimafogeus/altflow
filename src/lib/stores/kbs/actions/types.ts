import { BulletActionName } from '../bulletActions'
import { GlobalActionName } from '../globalActions'

export type ActionName = BulletActionName | GlobalActionName
