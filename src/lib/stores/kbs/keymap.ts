import { ActionName } from './actions/types'

const defaultKeymap: { [K in ActionName]: string } = {
	// bullet actions
	'complete-task': 'ctrl-enter',
	'toggle-focus-on-notes': 'shift-enter',
	'open-bullet-below': 'enter',
	indent: 'tab',
	unindent: 'shift-tab',
	'navigate-up': 'up',
	'navigate-down': 'down',
	backspace: 'backspace',
	'zoom-in': 'ctrl-.',
	'zoom-out': 'ctrl-,',
	'move-bullet-up': 'ctrl-shift-up',
	'move-bullet-down': 'ctrl-shift-down',
	'bullet-duplicate': 'ctrl-shift-d',
	'bullet-delete': 'ctrl-shift-backspace',
	'bullet-collapse': 'ctrl-up',
	'bullet-un-collapse': 'ctrl-down',
	'expression-tab-accept-suggestion': 'tab',

	// global
	cancel: 'escape',
	'toggle-focus-on-search': 'escape',
	'toggle-visibility-completed': 'ctrl-o',
	'navigate-home': "ctrl-'",
	'ctrl-k': 'ctrl-k',
}

const precedence: {} = {
	tab: ['']
}

export default defaultKeymap
