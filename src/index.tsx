import React from 'react'
import ReactDOM from 'react-dom'
import { Router } from 'react-router-dom'

import App from './App'
import { history } from './routes'

const render = Component =>
	ReactDOM.render(
		<Router history={history as any}>
			<Component />
		</Router>,
		document.getElementById('root')
	)

render(App)

if (module.hot) {
	module.hot.accept('./App', () => {
		const NextApp = require('./App').default
		render(NextApp)
	})
}
